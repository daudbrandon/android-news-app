package com.example.newsapp.data.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.example.newsapp.data.apis.NewsInterface
import com.example.newsapp.data.dataSource.TopNewsDataSourceFactory
import com.example.newsapp.data.dataSource.TopNewsNetworkDataSource
import com.example.newsapp.data.models.Article
import io.reactivex.disposables.CompositeDisposable

class TopNewsRepository(private val apiService: NewsInterface) {
    lateinit var topNewsPagedList: LiveData<PagedList<Article>>
    lateinit var topNewsDataSourceFactory: TopNewsDataSourceFactory

    fun fetchLiveTopNewsPagedList(compositeDisposable: CompositeDisposable, country : String):LiveData<PagedList<Article>>{
        topNewsDataSourceFactory= TopNewsDataSourceFactory(apiService,compositeDisposable,country)

        val config: PagedList.Config= PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(5)
            .build()
        topNewsPagedList= LivePagedListBuilder(topNewsDataSourceFactory, config).build()
        return  topNewsPagedList

    }
    fun getNetworkState(): LiveData<NetworkState>{
        return Transformations.switchMap<TopNewsNetworkDataSource,NetworkState>(
            topNewsDataSourceFactory.topNewsLiveDataService,TopNewsNetworkDataSource::networkState
        )
    }
}