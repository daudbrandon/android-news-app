package com.example.newsapp.data.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.example.newsapp.data.apis.NewsInterface
import com.example.newsapp.data.dataSource.NewsDataSourceFactory
import com.example.newsapp.data.dataSource.NewsNetworkDataSource
import com.example.newsapp.data.dataSource.TopNewsDataSourceFactory
import com.example.newsapp.data.dataSource.TopNewsNetworkDataSource
import com.example.newsapp.data.models.Article
import io.reactivex.disposables.CompositeDisposable

class NewsRepository(private val apiService: NewsInterface) {
    lateinit var newsPagedList: LiveData<PagedList<Article>>
    lateinit var newsDataSourceFactory: NewsDataSourceFactory

    fun fetchLiveNewsPagedList(compositeDisposable: CompositeDisposable, title : String, language: String):LiveData<PagedList<Article>>{
        newsDataSourceFactory= NewsDataSourceFactory(apiService,compositeDisposable,title,language)

        val config: PagedList.Config= PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(20)
            .build()
        newsPagedList= LivePagedListBuilder(newsDataSourceFactory, config).build()
        return  newsPagedList

    }
    fun getNetworkState(): LiveData<NetworkState>{
        return Transformations.switchMap<NewsNetworkDataSource,NetworkState>(
            newsDataSourceFactory.newsLiveDataService, NewsNetworkDataSource::networkState
        )
    }
}