package com.example.newsapp.data.apis

import com.example.newsapp.data.models.News
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsInterface {

    //https://newsapi.org/v2/top-headlines?country=us&apiKey=413ac79f2b6b47dab54ab73119aff3aa
//    https://newsapi.org/v2/everything?q=bitcoin&apiKey=413ac79f2b6b47dab54ab73119aff3aa

    @GET("top-headlines")
    fun getPopularNews(@Query("page") page: Int,
                       @Query("country")country: String): Single<News>

    @GET("everything")
    fun getAllNews(@Query("page") page: Int,
                   @Query("q")title: String,
                   @Query("language")language: String): Single<News>
}