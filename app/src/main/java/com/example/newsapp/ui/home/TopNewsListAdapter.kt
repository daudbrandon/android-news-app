package com.example.newsapp.ui.home

import android.annotation.SuppressLint
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.newsapp.data.models.Article
import com.example.newsapp.data.repositories.NetworkState
import com.example.newsapp.databinding.NetworkStateItemBinding
import com.example.newsapp.databinding.TopNewsLtemBinding
import java.text.SimpleDateFormat
import java.time.Instant
import java.util.*


class TopNewsListAdapter: PagedListAdapter<Article, RecyclerView.ViewHolder>(TopNewsDiffCallback())  {

    val TOP_NEWS = 1
    val NETWORK_VIEW_TYPE = 2
    private var networkState: NetworkState? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val topNewsBinding: TopNewsLtemBinding;
        val networkStateBinding: NetworkStateItemBinding;
        if (viewType == TOP_NEWS) {
            topNewsBinding= TopNewsLtemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return TopNewsItemViewHolder(topNewsBinding)
        } else {
             networkStateBinding = NetworkStateItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return NetworkStateItemViewHolder(networkStateBinding)
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(getItemViewType(position)== TOP_NEWS){
            (holder as TopNewsItemViewHolder).bind(getItem(position))
        }else{
            (holder as NetworkStateItemViewHolder).bind(networkState)
        }
    }

    private fun hasExtraRow(): Boolean {
        return networkState != null && networkState !== NetworkState.LOADED
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + if (hasExtraRow()) 1 else 0
    }

    override fun getItemViewType(position: Int): Int {
        return if (hasExtraRow() && position == itemCount - 1) {
            NETWORK_VIEW_TYPE
        } else {
            TOP_NEWS
        }
    }

    class TopNewsDiffCallback : DiffUtil.ItemCallback<Article>() {
        override fun areItemsTheSame(oldItem: Article, newItem: Article): Boolean {
            return oldItem.author == newItem.author
        }

        override fun areContentsTheSame(oldItem: Article, newItem: Article): Boolean {
            return oldItem == newItem
        }
    }

    @Suppress("DEPRECATION", "NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    class TopNewsItemViewHolder(private val topNewsBinding: TopNewsLtemBinding) : RecyclerView.ViewHolder(topNewsBinding.root) {

        @RequiresApi(Build.VERSION_CODES.O)
        @SuppressLint("SimpleDateFormat")
        fun bind(article: Article?){

            topNewsBinding.apply {
                topTitleItem.text=article?.title

                val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss") // your format
                val date: Date = format.parse(article?.publishedAt)
                topDateItem.text= SimpleDateFormat("MMM dd, yyyy").format(date)
                val moviePosterURL= article?.urlToImage
                Glide.with(itemView.context)
                    .load(moviePosterURL)
                    .into(topImageItem)

//                itemView.setOnClickListener{
//                    val intent = Intent(context, SingleMovie::class.java)
//                    intent.putExtra("id",movie?.id)
//                    context.startActivity(intent)
//                }
            }

        }
    }

    class NetworkStateItemViewHolder(private val networkStateBinding: NetworkStateItemBinding) : RecyclerView.ViewHolder(networkStateBinding.root) {

        fun bind(networkState: NetworkState?) {
            networkStateBinding.apply {
                if (networkState != null && networkState == NetworkState.LOADING) {
                    progressBarNetworkItem.visibility = View.VISIBLE;
                } else {
                    progressBarNetworkItem.visibility = View.GONE;
                }
                if (networkState != null && networkState == NetworkState.ERROR) {
                    txtErrorNetworkItem.visibility = View.VISIBLE;
                    txtErrorNetworkItem.text = networkState.msg;
                } else {
                    txtErrorNetworkItem.visibility = View.GONE;
                }
            }
        }
    }
    fun setNetworkState(newNetworkState: NetworkState){
        val previousState = this.networkState
        val hadExtraRow:Boolean = hasExtraRow()
        this.networkState = newNetworkState
        val hasExtraRow= hasExtraRow()

        if(hadExtraRow != hasExtraRow){
            if(hadExtraRow){
                notifyItemChanged(super.getItemCount())
            }else {
                notifyItemInserted(super.getItemCount())
            }
        }
        else if(hasExtraRow && previousState != newNetworkState){
            notifyItemChanged(itemCount-1)
        }
    }

}