package com.example.newsapp.data.repositories

enum class Status{
    RUNNING,
    SUCCESS,
    FAILED,
    NO_RESULT
}

class NetworkState(val status: Status, val msg: String)  {
    companion object{
        val LOADED: NetworkState = NetworkState(Status.SUCCESS, "Success")
        val LOADING: NetworkState = NetworkState(Status.RUNNING, "Running")
        val ERROR: NetworkState = NetworkState(Status.FAILED, "something went wrong")
        val NO_DATA: NetworkState = NetworkState(Status.NO_RESULT, "No Result")
    }
}