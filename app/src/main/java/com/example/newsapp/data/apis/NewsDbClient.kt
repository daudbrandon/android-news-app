package com.example.newsapp.data.apis

import io.reactivex.Single
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import java.util.concurrent.TimeUnit

const val API_KEY= "413ac79f2b6b47dab54ab73119aff3aa"
const val BASE_URL= "https://newsapi.org/v2/"
const val FIRST_PAGE=1
const val COUNTRY="us"
//https://newsapi.org/v2/top-headlines?country=us&apiKey=413ac79f2b6b47dab54ab73119aff3aa
//https://newsapi.org/v2/everything?apiKey=413ac79f2b6b47dab54ab73119aff3aa

object NewsDbClient{
    fun getClient():NewsInterface{
        val requestInterceptor= Interceptor{ chain ->
            val url= chain.request()
                .url()
                .newBuilder()
                .addQueryParameter("apiKey", API_KEY)
                .build()

            val request= chain.request()
                .newBuilder()
                .url(url)
                .build()

            return@Interceptor chain.proceed(request)
        }
        val okHttpClient= OkHttpClient.Builder()
            .addInterceptor(requestInterceptor)
            .connectTimeout(60, TimeUnit.SECONDS)
            .build()
        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(NewsInterface::class.java)

    }

}
