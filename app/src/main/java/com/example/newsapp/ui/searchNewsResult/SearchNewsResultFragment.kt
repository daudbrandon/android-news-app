package com.example.newsapp.ui.searchNewsResult

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import com.example.newsapp.data.apis.NewsDbClient
import com.example.newsapp.data.apis.NewsInterface
import com.example.newsapp.data.repositories.NetworkState
import com.example.newsapp.data.repositories.NewsRepository
import com.example.newsapp.data.repositories.TopNewsRepository
import com.example.newsapp.data.viewModels.HomeViewModel
import com.example.newsapp.data.viewModels.SearchNewsResultViewModel
import com.example.newsapp.databinding.FragmentSearchNewsResultBinding
import com.example.newsapp.ui.home.TopNewsListAdapter


class SearchNewsResultFragment : Fragment() {
    private lateinit var viewModel: SearchNewsResultViewModel
    lateinit var  NewsRepository: NewsRepository
    private var _binding: FragmentSearchNewsResultBinding? = null
    private val binding get() = _binding!!

    val args: SearchNewsResultFragmentArgs by navArgs()
    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentSearchNewsResultBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val apiService: NewsInterface = NewsDbClient.getClient()
        NewsRepository = NewsRepository(apiService)

        viewModel = getViewModel(args.newsContent.toString(),args.countryCode.toString())

    val newsAdapter = SearchNewsResultListAdapter()

    val gridLayoutManager= GridLayoutManager(requireContext(),1)

    gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup(){
        override fun getSpanSize(position: Int): Int {
            return 1
        }
    }

    binding.apply {
        rvSearchNewsResult.layoutManager=gridLayoutManager
        rvSearchNewsResult.setHasFixedSize(true)
        rvSearchNewsResult.adapter=newsAdapter

        viewModel.newsPagedList.observe(viewLifecycleOwner, Observer {
            newsAdapter.submitList(it)
        })
        viewModel.networkState.observe(viewLifecycleOwner, Observer {
            searchNewsBar.visibility=if(viewModel.listIsEmpty() && (it == NetworkState.LOADING)) View.VISIBLE else View.GONE
            searchNewsError.visibility= if(viewModel.listIsEmpty() && it == NetworkState.ERROR) View.VISIBLE else View.GONE
            tvNoResult.visibility=if(viewModel.listIsEmpty() && (it == NetworkState.NO_DATA)) View.VISIBLE else View.GONE

            if(!viewModel.listIsEmpty()){
                newsAdapter.setNetworkState(it)
            }
        })

    }


        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun getViewModel(title: String,language: String): SearchNewsResultViewModel {
        return ViewModelProviders.of(this, object : ViewModelProvider.Factory{
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                @Suppress("UNCHECKED_CAST")
                return SearchNewsResultViewModel(NewsRepository,title,language) as T
            }

        })[SearchNewsResultViewModel::class.java]
    }

}