package com.example.newsapp.ui.searchNewsResult

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.newsapp.data.models.Article
import com.example.newsapp.data.repositories.NetworkState
import com.example.newsapp.databinding.NetworkStateItemBinding
import com.example.newsapp.databinding.SearchNewsResultItemBinding
import com.example.newsapp.databinding.TopNewsLtemBinding
import com.example.newsapp.ui.home.TopNewsListAdapter
import java.text.SimpleDateFormat
import java.util.*

class SearchNewsResultListAdapter: PagedListAdapter<Article, RecyclerView.ViewHolder>(SearchNewsResultDiffCallback()) {

    val NEWS = 1
    val NETWORK_VIEW_TYPE = 2
    private var networkState: NetworkState? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val searchNewsResultBinding: SearchNewsResultItemBinding;
        val networkStateBinding: NetworkStateItemBinding;
        if (viewType == NEWS) {
            searchNewsResultBinding= SearchNewsResultItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return NewsItemViewHolder(searchNewsResultBinding)
        } else {
            networkStateBinding = NetworkStateItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return NetworkStateItemViewHolder(networkStateBinding)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(getItemViewType(position)== NEWS){
            (holder as NewsItemViewHolder).bind(getItem(position))
        }else{
            (holder as NetworkStateItemViewHolder).bind(networkState)
        }
    }

    private fun hasExtraRow(): Boolean {
        return networkState != null && networkState !== NetworkState.LOADED
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + if (hasExtraRow()) 1 else 0
    }

    override fun getItemViewType(position: Int): Int {
        return if (hasExtraRow() && position == itemCount - 1) {
            NETWORK_VIEW_TYPE
        } else {
            NEWS
        }
    }
    class SearchNewsResultDiffCallback : DiffUtil.ItemCallback<Article>() {
        override fun areItemsTheSame(oldItem: Article, newItem: Article): Boolean {
            return oldItem.author == newItem.author
        }

        override fun areContentsTheSame(oldItem: Article, newItem: Article): Boolean {
            return oldItem == newItem
        }
    }

    @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    class NewsItemViewHolder(private val newsBinding: SearchNewsResultItemBinding) : RecyclerView.ViewHolder(newsBinding.root) {
        @SuppressLint("SimpleDateFormat")
        fun bind(article: Article?){

            newsBinding.apply {
                tvSearchResultTitleItem.text=article?.title

                val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss") // your format
                val date: Date = format.parse(article?.publishedAt)
                tvSearchResultDateItem.text= SimpleDateFormat("MMM dd, yyyy").format(date)
                val moviePosterURL= article?.urlToImage
                Glide.with(itemView.context)
                    .load(moviePosterURL)
                    .into(ivSearchResultImageItem)
            }

        }
    }
    class NetworkStateItemViewHolder(private val networkStateBinding: NetworkStateItemBinding) : RecyclerView.ViewHolder(networkStateBinding.root) {

        fun bind(networkState: NetworkState?) {
            networkStateBinding.apply {
                if (networkState != null && networkState == NetworkState.LOADING) {
                    progressBarNetworkItem.visibility = View.VISIBLE;
                } else {
                    progressBarNetworkItem.visibility = View.GONE;
                }
                if (networkState != null && networkState == NetworkState.ERROR) {
                    txtErrorNetworkItem.visibility = View.VISIBLE;
                    txtErrorNetworkItem.text = networkState.msg;
                } else {
                    txtErrorNetworkItem.visibility = View.GONE;
                }
                if(networkState != null && networkState == NetworkState.NO_DATA){
                    txtErrorNetworkItem.text = networkState.msg;
                }
            }
        }
    }
    fun setNetworkState(newNetworkState: NetworkState){
        val previousState = this.networkState
        val hadExtraRow:Boolean = hasExtraRow()
        this.networkState = newNetworkState
        val hasExtraRow= hasExtraRow()

        if(hadExtraRow != hasExtraRow){
            if(hadExtraRow){
                notifyItemChanged(super.getItemCount())
            }else {
                notifyItemInserted(super.getItemCount())
            }
        }
        else if(hasExtraRow && previousState != newNetworkState){
            notifyItemChanged(itemCount-1)
        }
    }
}