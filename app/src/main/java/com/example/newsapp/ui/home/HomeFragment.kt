@file:Suppress("DEPRECATION")

package com.example.newsapp.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.example.newsapp.data.apis.NewsDbClient
import com.example.newsapp.data.apis.NewsInterface
import com.example.newsapp.data.repositories.NetworkState
import com.example.newsapp.data.repositories.TopNewsRepository
import com.example.newsapp.data.viewModels.HomeViewModel
import com.example.newsapp.databinding.FragmentHomeBinding

@Suppress("DEPRECATION")
class HomeFragment : Fragment() {

    private lateinit var viewModel: HomeViewModel
    lateinit var  topNewsRepository: TopNewsRepository
    private var _binding: FragmentHomeBinding? = null


    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val apiService: NewsInterface =NewsDbClient.getClient()
        topNewsRepository = TopNewsRepository(apiService)
        viewModel = getViewModel("us")

        val topNewsAdapter = TopNewsListAdapter()

        val gridLayoutManager= GridLayoutManager(requireContext(),1)

        gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup(){
            override fun getSpanSize(position: Int): Int {
                return 1
            }

        }

        binding.apply {
            rvTopNews.layoutManager=gridLayoutManager
            rvTopNews.setHasFixedSize(true)
            rvTopNews.adapter=topNewsAdapter

            viewModel.topNewsPagedList.observe(viewLifecycleOwner, Observer {
                topNewsAdapter.submitList(it)
            })
            viewModel.networkState.observe(viewLifecycleOwner, Observer {
                popularBar.visibility=if(viewModel.listIsEmpty() && it == NetworkState.LOADING) View.VISIBLE else View.GONE
                popularError.visibility= if(viewModel.listIsEmpty() && it == NetworkState.ERROR) View.VISIBLE else View.GONE

                if(!viewModel.listIsEmpty()){
                    topNewsAdapter.setNetworkState(it)
                }
            })

        }
        return root
    }
    private fun getViewModel(country: String): HomeViewModel {
        return ViewModelProviders.of(this, object : ViewModelProvider.Factory{
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                @Suppress("UNCHECKED_CAST")
                return HomeViewModel(topNewsRepository,country) as T
            }

        })[HomeViewModel::class.java]
    }
}