package com.example.newsapp.data.dataSource

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.example.newsapp.data.apis.COUNTRY
import com.example.newsapp.data.apis.FIRST_PAGE
import com.example.newsapp.data.apis.NewsInterface
import com.example.newsapp.data.models.Article
import com.example.newsapp.data.models.News
import com.example.newsapp.data.repositories.NetworkState
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class TopNewsNetworkDataSource(private val apiService: NewsInterface, private val compositeDisposable: CompositeDisposable,country: String): PageKeyedDataSource<Int, Article>(){
    private var page =  FIRST_PAGE
  private var COUNTRY=country
    val networkState: MutableLiveData<NetworkState> = MutableLiveData()
    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Article>
    ) {

        networkState.postValue(NetworkState.LOADING)

        compositeDisposable.add(
            apiService.getPopularNews(page,COUNTRY)
                .subscribeOn(Schedulers.io())
                .subscribe({
                    callback.onResult(it.articles,null, page+1)
                    networkState.postValue(NetworkState.LOADED)
                },{
                    networkState.postValue(NetworkState.ERROR)
                    Log.e("TopNewsDataSource", it.message.toString())
                })
        )

    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Article>) {

    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Article>) {
        networkState.postValue(NetworkState.LOADING)

        compositeDisposable.add(
            apiService.getPopularNews(params.key,COUNTRY)
                .subscribeOn(Schedulers.io())
                .subscribe({
                    if(it.totalResults>=params.key){
                        callback.onResult(it.articles,params.key)
                        networkState.postValue(NetworkState.LOADED)
                    }
                },{
                    networkState.postValue(NetworkState.ERROR)
                    Log.e("TopNewsDataSource", it.message.toString())
                })
        )
    }
}