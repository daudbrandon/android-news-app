package com.example.newsapp.ui.searchNews

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.newsapp.R
import com.example.newsapp.data.models.Country
import com.example.newsapp.databinding.FragmentSearchNewsBinding


@Suppress("DEPRECATION")
class SearchNewsFragment : Fragment() {


    private var _binding: FragmentSearchNewsBinding? = null
    private val binding get() = _binding!!
    var countryCode= ""
        override fun onResume() { //use this to prevent the item not showing when changing fragment
        super.onResume()

            //initiate the data
            val countries =listOf(
                Country("fr", "French"),
                Country("de", "Germany"),
                Country("ru", "Russia"),
                Country("us", "United States")
            )
            val countriesName = countries.map {
                it.country
            }

            //set adapter of dropdown
            val countriesArrayAdapter=ArrayAdapter(requireContext(),R.layout.country_list_item,countriesName)
            binding.autoCompleteCountry.setAdapter(countriesArrayAdapter)

            //get the country code
            binding.autoCompleteCountry.onItemClickListener =
                AdapterView.OnItemClickListener { parent, view, position, id ->
                    countryCode = countries[position].code
                }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentSearchNewsBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.btnSearchNews.setOnClickListener {
            val bundle = bundleOf("countryCode" to countryCode,
                "newsContent" to binding.etNewsContain.text.toString())
            view?.findNavController()?.navigate(R.id.action_fragmentSearchNews_to_fragmentSearchNewsResult, bundle)
        }

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}