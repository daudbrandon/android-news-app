package com.example.newsapp.ui.splashScreen

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.*
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.newsapp.R



@Suppress("DEPRECATION")
@SuppressLint("CustomSplashScreen")
class SplashScreenFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

val view= inflater.inflate(R.layout.fragment_splash_screen, container, false)

    Handler(Looper.myLooper()!!).postDelayed({
        findNavController().navigate(R.id.action_fragmentSplashScreen_to_fragmentHome)
    },3000)
        return view
    }

}