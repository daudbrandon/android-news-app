package com.example.newsapp.data.dataSource

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.example.newsapp.data.apis.NewsInterface
import com.example.newsapp.data.models.Article
import io.reactivex.disposables.CompositeDisposable

class NewsDataSourceFactory(private val apiService: NewsInterface, private val compositeDisposable: CompositeDisposable, title: String,language:String): DataSource.Factory<Int, Article>()  {

    private var TITLE=title
    private var LANGUAGE=language
    val newsLiveDataService= MutableLiveData<NewsNetworkDataSource>()


    override fun create(): DataSource<Int, Article> {

        val newsDataSource = NewsNetworkDataSource(apiService,compositeDisposable, TITLE,LANGUAGE )

        newsLiveDataService.postValue(newsDataSource)
        return newsDataSource
    }
}