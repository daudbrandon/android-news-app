package com.example.newsapp.data.dataSource

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.example.newsapp.data.apis.NewsInterface
import com.example.newsapp.data.models.Article
import com.example.newsapp.data.models.News
import io.reactivex.disposables.CompositeDisposable

class TopNewsDataSourceFactory(private val apiService: NewsInterface, private val compositeDisposable: CompositeDisposable, country: String): DataSource.Factory<Int, Article>() {

    private var COUNTRY=country
    val topNewsLiveDataService= MutableLiveData<TopNewsNetworkDataSource>()
    override fun create(): DataSource<Int, Article> {

        val topNewsDataSource = TopNewsNetworkDataSource(apiService,compositeDisposable, COUNTRY )

        topNewsLiveDataService.postValue(topNewsDataSource)
        return topNewsDataSource
    }
}