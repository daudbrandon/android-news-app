package com.example.newsapp.data.dataSource

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.example.newsapp.data.apis.FIRST_PAGE
import com.example.newsapp.data.apis.NewsInterface
import com.example.newsapp.data.models.Article
import com.example.newsapp.data.repositories.NetworkState
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class NewsNetworkDataSource(private val apiService: NewsInterface, private val compositeDisposable: CompositeDisposable, title: String, language: String): PageKeyedDataSource<Int, Article>() {

    private var page =  FIRST_PAGE
    private var LANGUAGE=language
    private var TITLE=title
    val networkState: MutableLiveData<NetworkState> = MutableLiveData()

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Article>
    ) {

        networkState.postValue(NetworkState.LOADING)

        compositeDisposable.add(
            apiService.getAllNews(page,TITLE,LANGUAGE)
                .subscribeOn(Schedulers.io())
                .subscribe({
                    if(it.totalResults==0){
                        networkState.postValue(NetworkState.NO_DATA)
                    }else{
                        callback.onResult(it.articles,null, page+1)
                        networkState.postValue(NetworkState.LOADED)
                    }

                },{
                    networkState.postValue(NetworkState.ERROR)
                    Log.e("NewsDataSource", it.message.toString())
                })
        )
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Article>) {
        TODO("Not yet implemented")
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Article>) {
        networkState.postValue(NetworkState.LOADING)

        compositeDisposable.add(
            apiService.getAllNews(params.key,TITLE,LANGUAGE)
                .subscribeOn(Schedulers.io())
                .subscribe({
                    if(it.totalResults>=params.key){
                        callback.onResult(it.articles,params.key)
                        networkState.postValue(NetworkState.LOADED)
                    }else if(it.totalResults==0){
                        networkState.postValue(NetworkState.NO_DATA)
                    }
                },{
                    networkState.postValue(NetworkState.ERROR)
                    Log.e("NewsDataSource", it.message.toString())
                })
        )
    }
}