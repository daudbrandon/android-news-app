package com.example.newsapp.data.models


import com.google.gson.annotations.SerializedName

data class Country(
    val code: String,
    val country: String

)