package com.example.newsapp.data.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import com.example.newsapp.data.models.Article
import com.example.newsapp.data.repositories.NetworkState
import com.example.newsapp.data.repositories.TopNewsRepository
import io.reactivex.disposables.CompositeDisposable

class HomeViewModel(private val topNewsRepository: TopNewsRepository,country: String) : ViewModel() {

    private val compositeDisposable= CompositeDisposable()

    val topNewsPagedList: LiveData<PagedList<Article>> by lazy{
        topNewsRepository.fetchLiveTopNewsPagedList(compositeDisposable,country)
    }
    val networkState: LiveData<NetworkState> by lazy{
        topNewsRepository.getNetworkState()
    }

    fun listIsEmpty():Boolean{
        return topNewsPagedList.value?.isEmpty() ?:true
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}