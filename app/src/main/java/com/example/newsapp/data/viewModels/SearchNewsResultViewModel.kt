package com.example.newsapp.data.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import com.example.newsapp.data.models.Article
import com.example.newsapp.data.repositories.NetworkState
import com.example.newsapp.data.repositories.NewsRepository
import com.example.newsapp.data.repositories.TopNewsRepository
import io.reactivex.disposables.CompositeDisposable

class SearchNewsResultViewModel(private val newsRepository: NewsRepository,title: String, language: String) : ViewModel() {

    private val compositeDisposable= CompositeDisposable()

    val newsPagedList: LiveData<PagedList<Article>> by lazy{
        newsRepository.fetchLiveNewsPagedList(compositeDisposable,title,language)
    }
    val networkState: LiveData<NetworkState> by lazy{
        newsRepository.getNetworkState()
    }

    fun listIsEmpty():Boolean{
        return newsPagedList.value?.isEmpty() ?:true
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}